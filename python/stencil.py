#!/usr/bin/python3


###############################################################################
#    IMPORTS                                                                  #
###############################################################################
import sys
import os
import PIL.Image as pil
from datetime import datetime, timedelta



###############################################################################
#    ARGUMENTS                                                                #
###############################################################################
if len(sys.argv) == 1:
	exit("Paramètre manquant : image")

filepath_in = sys.argv[1]
filepath_out = os.path.abspath(filepath_in.replace("in", "out")).replace(".", ".out.")

print(f"Input:  {filepath_in}")
print(f"Output: {filepath_out}")



###############################################################################
#    IMAGE                                                                    #
###############################################################################
img_in = None
try:
	img_in = pil.open(filepath_in)
except Exception as e:
	exit(e)

mode = img_in.mode
width, height = img_in._size
img_out = pil.new(mode, (width, height))

print(f"Mode:   {mode}")
print(f"Width:  {width}")
print(f"Height: {height}")



###############################################################################
#    TRAITEMENT                                                               #
###############################################################################
matrix_transformation = [
	 0, -1/5, 0,
	 0, 1, -1/5,
	 1.5, 0, 0
]

chrono_start = datetime.now()
for i in range(width):
	for j in range(height):
		px_mc = img_in.getpixel((i, j))

		# si le pixel courant est sur la bordure de l'image
		if j in {0, height-1} or i in {0, width-1}:
			img_out.putpixel((i, j), px_mc)

		# si le pixel courant n'est pas sur la bordure de l'image
		else:
			px_tl = img_in.getpixel((i-1, j-1))
			px_tc = img_in.getpixel((i, j-1))
			px_tr = img_in.getpixel((i+1, j-1))
			px_ml = img_in.getpixel((i-1, j))
			px_mr = img_in.getpixel((i+1, j))
			px_bl = img_in.getpixel((i-1, j+1))
			px_bc = img_in.getpixel((i, j+1))
			px_br = img_in.getpixel((i+1, j+1))

			red = matrix_transformation[0] * px_tl[0] + \
				matrix_transformation[1] * px_tc[0] + \
				matrix_transformation[2] * px_tr[0] + \
				matrix_transformation[3] * px_ml[0] + \
				matrix_transformation[4] * px_mc[0] + \
				matrix_transformation[5] * px_mr[0] + \
				matrix_transformation[6] * px_bl[0] + \
				matrix_transformation[7] * px_bc[0] + \
				matrix_transformation[8] * px_br[0]
			
			green = matrix_transformation[0] * px_tl[1] + \
				matrix_transformation[1] * px_tc[1] + \
				matrix_transformation[2] * px_tr[1] + \
				matrix_transformation[3] * px_ml[1] + \
				matrix_transformation[4] * px_mc[1] + \
				matrix_transformation[5] * px_mr[1] + \
				matrix_transformation[6] * px_bl[1] + \
				matrix_transformation[7] * px_bc[1] + \
				matrix_transformation[8] * px_br[1]
			
			blue = matrix_transformation[0] * px_tl[2] + \
				matrix_transformation[1] * px_tc[2] + \
				matrix_transformation[2] * px_tr[2] + \
				matrix_transformation[3] * px_ml[2] + \
				matrix_transformation[4] * px_mc[2] + \
				matrix_transformation[5] * px_mr[2] + \
				matrix_transformation[6] * px_bl[2] + \
				matrix_transformation[7] * px_bc[2] + \
				matrix_transformation[8] * px_br[2]
			
			if red < 0: red = 0
			if green < 0: green = 0
			if blue < 0: blue = 0

			if red > 255: red = 255
			if green > 255: green = 255
			if blue > 255: blue = 255

			if mode == "RGBA":
				px_new = (int(red), int(green), int(blue), px_mc[3])
			else:
				px_new = (int(red), int(green), int(blue))
	
	
			img_out.putpixel((i, j), px_new)
chrono_stop = datetime.now()

# affichage du temps d'exécution
print(f"Time:   {(chrono_stop - chrono_start) / timedelta(microseconds=1000)}ms")



###############################################################################
#    SAVE                                                                     #
###############################################################################
img_out.save(filepath_out)