#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <string>
#include <opencv2/opencv.hpp>



/******************************************************************************
*	CONSTANTS                                                                 *
******************************************************************************/
const unsigned int MODE_RGB  = 3;
const unsigned int MODE_RGBA = 4;

const double matrix_trans[9] = {
	0.0, -0.1,  0.0,
	0.0,  0.6, -0.1,
	1.0,  0.0,  0.0
};



/******************************************************************************
*	FUNCTION                                                                  *
******************************************************************************/
void Traitement(uchar* data_in, uchar* data_out, unsigned int width, unsigned int height, unsigned int mode, const double* matrix_trans) {
	for (unsigned int i = 0; i < height; i++) {
		for (unsigned int j = 0; j < width*mode; j += mode) {
			
			unsigned int pos_global = i * width * mode + j;

			uchar comp_blue  = data_in[pos_global];
			uchar comp_green = data_in[pos_global+1];
			uchar comp_red   = data_in[pos_global+2];
			uchar comp_alpha = mode == MODE_RGBA ? data_in[pos_global+3] : 255;
			
			if (i == 0 || i == height-1 || j == 0 || j == (width-1)*mode ) {
				data_out[pos_global]   = comp_blue;
				data_out[pos_global+1] = comp_green;
				data_out[pos_global+2] = comp_red;

				if (mode == MODE_RGBA) { data_out[pos_global+3] = comp_alpha; }
			}
			else {
				double coef = 0.0;
				unsigned int pos_local = 0;

				double blue  = 0.0;
				double green = 0.0;
				double red   = 0.0;

				// calcul de la nouvelle couleur du pixel courant en fonction des voisins
				for (int a = -1; a < 2; a++) {
					for (int b = -3; b < (int)mode+1; b += mode) {
						coef = matrix_trans[(1+a)*mode + (mode+b)/mode];
						pos_local = (i+a) * width * mode + (j+b);
						
						blue  += coef * (int)data_in[pos_local];
						green += coef * (int)data_in[pos_local+1];
						red   += coef * (int)data_in[pos_local+2];
					}
				}

				// normaliser les valeurs pour rester dans la borne [0:255]				
				blue  = blue  < 0 ? 0 : blue  > 255 ? 255 : blue;
				green = green < 0 ? 0 : green > 255 ? 255 : green;
				red   = red   < 0 ? 0 : red   > 255 ? 255 : red;

				// sauvegarder le nouveau pixel
				data_out[pos_global]   = (uchar)blue;
				data_out[pos_global+1] = (uchar)green;
				data_out[pos_global+2] = (uchar)red;
				if (mode == MODE_RGBA) { data_out[pos_global+3] = comp_alpha; }
			}
		}
	}
}




/******************************************************************************
*	MAIN                                                                      *
******************************************************************************/
int main(int argc, char* argv[]) {

	if (argc != 2) {
		printf("Erreur de paramètre.");
		return EXIT_FAILURE;
	}

	// récupération du chemin d'accès à l'image d'entrée (fournie en paramètre)
	char* filepath_in = realpath(argv[1], NULL);
	if (filepath_in == NULL) {
		printf("Erreur de fichier: cette image n'existe pas ou n'est pas accésible.");
		return EXIT_FAILURE;
	}

	// définir le fichier de sortie
	std::string filepath_out(filepath_in);
	int index = filepath_out.find("img/in");
	filepath_out.replace(index, 6, "img/out");
	index = filepath_out.find_last_of(".");
	filepath_out.replace(index, 1, ".out.");

	// création des images d'entrée et de sortie
	cv::Mat img_in  = cv::imread(filepath_in, cv::IMREAD_UNCHANGED);
	cv::Mat img_out = img_in.clone();

	// définition des dimentions de l'image et du mode
	const unsigned int width = img_in.cols;
	const unsigned int height = img_in.rows;
	const unsigned int mode = img_in.elemSize();

	if (width == 0 || height == 0) { return EXIT_FAILURE; }

	std::cout << "Input:  " << filepath_in << std::endl;
	std::cout << "Output: " << filepath_out << std::endl;
	if (mode == MODE_RGB) {
		std::cout << "Mode:   RGB" << std::endl;
	}
	else {
		std::cout << "Mode:   RGBA" << std::endl;
	}
	std::cout << "Width:  " << width << std::endl;
	std::cout << "Height: " << height << std::endl;

	// récupérer les données de l'image d'entrée
	uchar* data_in = img_in.data;
	uchar* data_out = img_out.data;

	// démarrage du chrono
	auto chrono_start = std::chrono::system_clock::now();

	// traitement
	Traitement(data_in, data_out, width, height, mode, matrix_trans);

	// arrêt du chrono
	auto chrono_stop = std::chrono::system_clock::now();

	// calcul le temps d'exécution et l'affiche
	auto time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_stop - chrono_start).count();
	std::cout << "Time:   " << time_ms << "ms" << std::endl;

	// sauvegarder l'image de sortie
	cv::imwrite(filepath_out, img_out);

	// désallocation
	img_in.deallocate();
	img_out.deallocate();

	return EXIT_SUCCESS;
}