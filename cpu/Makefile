### Répertoires
MOD_DIR := ./
SRC_DIR := src/
INC_DIR := include/
OBJ_DIR := obj/
EXE_DIR := bin/
DOC_DIR := doc/

### Extensions
SRC_EXT := .cpp
INC_EXT := .hpp
OBJ_EXT := .o

### Liste des fichiers
SRC := $(shell find $(MOD_DIR)$(SRC_DIR) -name *$(SRC_EXT))
OBJ := $(patsubst $(MOD_DIR)$(SRC_DIR)%$(SRC_EXT),$(MOD_DIR)$(OBJ_DIR)%$(OBJ_EXT),$(SRC))
EXE := main
DOC := Doxyfile

### Librairies
LIBS := `pkg-config --libs opencv`

### Compilateur
CPPC := g++
CPPFLAGS := -march=native -std=c++11 -lpthread -Wall -Wextra -Wpedantic -pedantic -ggdb -Wno-unused-but-set-parameter -Wno-unused-variable -Wno-unused-parameter -Wno-abi -iquote $(MOD_DIR)$(INC_DIR)

### Documentation
DOCGEN := doxygen


###############################################################################
### COULEURS ET STYLE

COLOR_NOIR := 30m
COLOR_ROUGE := 31m
COLOR_VERT := 32m
COLOR_ORANGE := 33m
COLOR_BLEU := 34m
COLOR_MAGENTA := 35m
COLOR_CYAN := 36m
COLOR_BLANC := 37m

FOND_NOIR := 40m
FOND_ROUGE := 41m
FOND_VERT := 42m
FOND_ORANGE := 43m
FOND_BLEU := 44m
FOND_MAGENTA := 45m
FOND_CYAN := 46m
FOND_BLANC := 47m

STYLE_NORMAL := \e[00;
STYLE_GRAS := \e[01;
STYLE_SOUS_LIGNE := \e[04;
STYLE_CLIGNOTANT := \e[05;
STYLE_SUR_LIGNE := \e[07;

RESET_ALL := \e[00;m
RESET_STYLE := \e[00;
RESET_COLOR := m
RESET_FOND := m

FORMAT_LINK := $(STYLE_SUR_LIGNE)$(COLOR_CYAN)LINK:$(RESET_ALL)
FORMAT_COMP := $(STYLE_SUR_LIGNE)$(COLOR_BLANC)COMP:$(RESET_ALL)
FORMAT_RUN := $(STYLE_SUR_LIGNE)$(COLOR_VERT)RUN:$(RESET_ALL)
FORMAT_MEM := $(STYLE_SUR_LIGNE)$(COLOR_MAGENTA)MEM:$(RESET_ALL)
FORMAT_DOC := $(STYLE_SUR_LIGNE)$(COLOR_BLEU)DOC:$(RESET_ALL)
FORMAT_CLEAN := $(STYLE_SUR_LIGNE)$(COLOR_ORANGE)CLEAN:$(RESET_ALL)
FORMAT_ERROR := $(STYLE_SUR_LIGNE)$(COLOR_ROUGE)ERROR:$(RESET_ALL)
FORMAT_ARROW := $(STYLE_GRAS)$(COLOR_BLANC) -> $(RESET_ALL)



###############################################################################
### CREATION DES REPERTOIRES

# Cette fonction prend en paramètre la cible (avec un chemin relatif) et
# regarde si l'arborécence existe déjà. Si elle existe, il ne fait rien. Si
# elle n'existe pas, pour éviter les problèmes de compilation, la fonction crée
# les répertoires afin d'accueil le fichier cible.
define copy_tree = 
$(shell if [ ! -d $(shell dirname $(1)) ]; then mkdir -p `dirname $(1)`; fi)
endef


# Cette fonction crée le répertoire passé en paramètre. Elle est utilisé pour
# s'assurer que les dossiers utils au projet soit présent avant le dépot ou la
# création d'un quelconque fichier (évite les erreurs de compilation).
define create_directory = 
$(shell if [ ! -d $(1) ]; then mkdir $(1); fi)
endef



###############################################################################
### REGLES DE COMPILATION

all : $(MOD_DIR)$(EXE_DIR)$(EXE)

### De l'objet à l'exécutable
$(MOD_DIR)$(EXE_DIR)$(EXE) : $(OBJ)
	$(call create_directory, $(MOD_DIR)$(EXE_DIR))
	@$(CPPC) $(CPPFLAGS) -o $@ $^ -lm $(LIBS)
	@$(foreach file,$^,echo "$(FORMAT_LINK) $(file) $(FORMAT_ARROW) $@";)



### Du code source à de l'objet
$(MOD_DIR)$(OBJ_DIR)%$(OBJ_EXT) : $(MOD_DIR)$(SRC_DIR)%$(SRC_EXT)
	$(call copy_tree, $@)
	@$(CPPC) $(CPPFLAGS) -o $@ -c $<
	@echo "$(FORMAT_COMP) $< $(FORMAT_ARROW) $@"




###############################################################################
### AUTRES OPTIONS

.PHONY : run clean memory doc

### Exécution du programme
run : $(MOD_DIR)$(EXE_DIR)$(EXE)
	@echo "$(FORMAT_RUN) $(MOD_DIR)$(EXE_DIR)$(EXE)"
	@$(MOD_DIR)$(EXE_DIR)$(EXE)




### Nettoyage du projet
clean :
	@$(foreach file,$(OBJ),if [ -f $(file) ]; then rm $(file); echo "$(FORMAT_CLEAN) $(file)"; fi;)
	@if [ -f $(MOD_DIR)$(EXE_DIR)$(EXE) ]; then rm $(MOD_DIR)$(EXE_DIR)$(EXE) && echo "$(FORMAT_CLEAN) $(MOD_DIR)$(EXE_DIR)$(EXE)"; fi




### Gestion de la mémoire
memory : $(MOD_DIR)$(EXE_DIR)$(EXE)
	@echo "$(FORMAT_MEM) $(MOD_DIR)$(EXE_DIR)$(EXE)"
	valgrind --track-origins=yes $(MOD_DIR)$(EXE_DIR)$(EXE)




### Documentation
doc :
ifeq ($(shell if [ -f $(DOC) ]; then echo 1; else echo 0; fi), 1)
	$(call create_directory, $(MOD_DIR)$(DOC_DIR))
	$(DOCGEN) $(DOC)
	make -C $(DOC_DIR)latex/
else
	@echo "$(FORMAT_ERROR) Le fichier $(DOC) n'existe pas."
endif
	@$(foreach file,$(wildcard $(MOD_DIR)$(DOC_DIR)*),echo "$(FORMAT_DOC) $(file)";)
