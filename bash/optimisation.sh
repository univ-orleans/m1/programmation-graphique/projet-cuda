#!/bin/bash

#####################################
# Data								#
#####################################
# le fichier executable
declare -x EXECUTABLE_FILE_PATH;

# l'image
declare -x IMAGE_FILE_PATH;

#####################################
# Args								#
#####################################
# récupère les chemins
if [[ $# -ne 2 ]]; then
    echo "error command : ./optimisation.sh [executable path] [image path]";
else
	EXECUTABLE_FILE_PATH=$1;
	IMAGE_FILE_PATH=$2;
fi;

#
echo "Times with different block size" > out.txt


for x in {4..128..4}
do
	for y in {4..128..4}
	do
	if [[ x*y -le 1024 ]]; then
		$EXECUTABLE_FILE_PATH $IMAGE_FILE_PATH $x $y >> out.txt
	fi;
    done
done
