# Projet Cuda

**date :** 16 février 2021 au 18 avril 2021  
**sujets :** [projets_CUDA.pdf](projets_CUDA.pdf)  
**projet :** Stencil  
**langage :** CUDA  

---

<br/><br/>

## Table des matières
[[_TOC_]]

<br/><br/>

## Objectif
Le stencil, ou plus communément appelé convolution, est un procédé permettant de réaliser un traitement (par exemple sur une image) dont la valeur calculée dépend de ses voisins et d’une matrice de convolution (coefficient à appliquer).

L’objectif du projet de programmation graphique (partie CUDA) est de réaliser l’ optimisation de calcul (ici traitement d’image) en utilisant les performances d’une carte graphique plutôt que du CPU.

Pour le projet, nous avons arbitrairement choisi une matrice de convolution de taille 3×3 qui éclaircit un peu plus l’image. L’effet se rapproche plus d’une exposition lumineuse plus prononcée que d’un décalage vers la couleur blanche. La matrice de convolution est définit comme suite :

![Matrice de convolution](doc/img/matrice_de_convolution.png "Matrice de convolution")

Le traitement appliqué à chaque pixel consiste à faire la somme des multiplications des valeurs de chaque composante d’un pixel (RGB) pour chaque voisins plus le pixel en cours avec le coefficient correspondant dans la matrice de convolution.

![Animation d'un exemple de stencil](https://pic3.zhimg.com/v2-a337793e291aa374e940757144dfb3ae_b.gif "Animation d'un exemple de stencil")

Exemples de transformations avec la matrice précédente :
| image d’origine | image transformé |
| :--: | :--: |
| ![alt text](img/in/fleurs.jpg "Image originale") | ![alt text](img/out/fleurs.out.jpg "Image transformée") |
| ![alt text](img/in/music.jpg "Image originale") | ![alt text](img/out/music.out.jpg "Image transformée") |

<br/><br/>

## Programmation pour CPU
Dans un premier temps, nous avons réalisé un **script python** permettant de nous approprier le principe d’application du stencil. Une fois celui-ci maîtrisé, nous avons retranscrit le code en **c++**.

#### Temps d'exécution sur CPU
| Image | Taille de l'image | Python | C++ |
| :-- | --: | --: | --: |
| fleur.jpg | 5184x3456 | 3min 32s | 1s |
| in.jpg | 14400x7200 | 20min 41s | 8s | 
| topology.jpg | 20000x9997 | erreur<span style="color:red">*</span> | 15s |

erreur<span style="color:red">*</span><span style="color:grey"> : La librairie de lecture d'image ne supporte pas les images trops grandes.</span>

Nous pouvons voir que la version python est beaucoup plus lente que la version c++. Cela peut venir de la librairie que nous avons choisie ou simplement du fait que python est un langage interprété et non compilé.

<br/><br/>

## Programmation pour GPU
Pour tenter d’améliorer les résultats obtenus dans la partie CPU, nous utilisons la puissance de calcul disponible sur la carte graphique. Concrètement, cela consiste à transférer une image sur cette dernière, exécuter le même traitement en parallèle sur différents pixels, puis récupérer le résultat obtenu.

Chaque pixel se voit attribuer un thread. Si le pixel est une bordure de l’image, le pixel de l’image fournie en entrée est fidèlement recopié sur le pixel de l’image de sortie. À l'inverse, si le pixel n’est pas une bordure, alors nous appliquons le stencil dessus. Enfin si un thread alloué ne se voit pas attribuer un pixel (coordonnées du thread supérieurs aux limites de coordonnées des pixels de l’image), alors celui-ci ne fait rien.

#### Temps d'exécution sur GPU
| Image | Taille de l'image | Pour des blocs de 32×32
| :-- | --: | --: |
| fleur.jpg | 5184x3456 | 20.35ms |
| in.jpg | 14400x7200 | 117.53ms |
| topology.jpg | 20000x9997 | 205.80ms |

<br/><br/>

## Recherche d'optimisations
Afin d’améliorer les résultats obtenus en GPU, pour la même image, nous avons réalisé un petit test qui consiste à exécuter le code CUDA avec en paramètre des tailles de bloc différentes. Test illustré par le graphe ci-dessous pour l’image `topography.jpg` :

![Graphe de performance en fonction de la taille des blocs](doc/img/resultat_de_performances.jpg "Graphe de performance en fonction de la taille des blocs")

Le graphe de performance montre que de manière globale, le niveau de performance s'accroît avec la dimension des blocs. Attention, ce n’est qu’une tendance générale ! Dans cet exemple (tout comme les deux autres images), le meilleur résultat a été obtenu avec une configuration de bloc de taille 32×4.

<br/><br/>

## Comparaison des résultats
| Image | Taille de l'image | CPU | GPU | Gain de vitesse |
| :-- | --: | --: | --: | --: |
| fleur.jpg    | 5184x3456  | 1s  | 20.35ms  | ×50 |
| in.jpg       | 14400x7200 | 8s  | 117.53ms | ×68 |
| topology.jpg | 20000x9997 | 15s | 205.80ms | ×73 |

Nous pouvons voir que la version GPU est plus de 50 fois plus rapide que la version CPU. N’ayant pas atteint les limites de performances de la carte graphique, nous pouvons constater que plus l’image est grande (c’est-à-dire plus la charge de calcul est importante) et plus la différence du temps d’exécution du stencil est importante entre le CPU et le GPU.

<br/><br/>

## Exécution du code

Chaqu'un des srcipts ci-dessous prend au moin une image en paramètre. Les images à utiliser sont dans le sous-répertoire `img/in` et les images générées par les scripts sont placées dans le sous-répertoire `img/out`.

**Script Python**
```bash
$ cd projet-cuda/
$ ./python/stencil.py ./img/in/fleurs.jpg
```

**Script C++**
```bash
$ cd projet-cuda/cpu/
$ make
$ ./bin/main ../img/in/fleurs.jpg
```

**Script CUDA**
```bash
$ cd projet-cuda/gpu/
$ make
$ ./bin/main-cu ../img/in/fleurs.jpg 32 32 # taille du bloc en x et en y
```

<br/><br/>

## Prérequis Logiciel
| Logiciel | Version utilisé | Description |
| :-- | :-: | :-- |
| Python | 3.8 | Pour exécuter le script de test `stencil.py`. |
| C++ | 11 | Pour exécuter le script `main` à destination d'une exécution sur CPU. |
| CUDA | 9.2 | Pour exécuter le script `main-cu` à destination d'une exécution sur GPU. |
| OpenCV | 2.0 | Pour importer et exporter les images. |

<br/><br/>

## Prérequis matériel
| Matériel | version utilisé |
| :-- | :-: |
| Carte graphique NVIDIA | GeForce GTX 780 |

<br/><br/>

## Contributeurs  
- Arnaud ORLAY (@arnorlay) : Master 1 IMIS Informatique
- Nicolas ZHOU (@Zhou_Nicolas) : Master 1 IMIS Informatique
